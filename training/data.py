TRAIN_DATA_CURRENT_USER = [
    ("I like playing soccer.", {"entities": [(0, 1, "CURRENT_USER")]}),
    ("Can you help me with this?", {"entities": [(13, 15, "CURRENT_USER")]}),
    ("This is my favorite book.", {"entities": [(8, 10, "CURRENT_USER")]}),
    ("Is this mine or yours?", {"entities": [(7, 11, "CURRENT_USER")]}),
    ("When I grow up, I want to be a doctor.", {
     "entities": [(5, 6, "CURRENT_USER"), (18, 19, "CURRENT_USER")]}),
    ("My dog is so cute!", {"entities": [(0, 2, "CURRENT_USER")]}),
    ("I can't believe it's raining again.", {
     "entities": [(0, 1, "CURRENT_USER")]}),
    ("Are these cookies mine?", {"entities": [(18, 22, "CURRENT_USER")]}),
    ("I'm going to the store later.", {"entities": [(0, 1, "CURRENT_USER")]}),
    ("This is me in the picture.", {"entities": [(8, 10, "CURRENT_USER")]}),
    ("Can I have a slice of pizza?", {"entities": [(4, 5, "CURRENT_USER")]}),
    ("That's my bike over there.", {"entities": [(6, 8, "CURRENT_USER")]}),
    ("I've always loved reading.", {"entities": [(0, 1, "CURRENT_USER")]}),
    ("Is this your jacket or mine?", {"entities": [(22, 26, "CURRENT_USER")]}),
    ("My favorite color is blue.", {"entities": [(0, 2, "CURRENT_USER")]}),
    ("I'm not feeling well today.", {"entities": [(0, 1, "CURRENT_USER")]}),
    ("When I was a child, I used to play soccer.", {
     "entities": [(5, 6, "CURRENT_USER"), (19, 20, "CURRENT_USER")]}),
    ("Is this your pencil or mine?", {"entities": [(21, 25, "CURRENT_USER")]}),
    ("I'll call you later.", {"entities": [(0, 1, "CURRENT_USER")]}),
    ("This book is mine, not yours.", {
     "entities": [(12, 16, "CURRENT_USER")]}),
    ("I should start exercising more often.",
     {"entities": [(0, 1, "CURRENT_USER")]}),
    ("Give it to me, please.", {"entities": [(11, 13, "CURRENT_USER")]}),
    ("Can you tell me the time?", {"entities": [(12, 14, "CURRENT_USER")]}),
    ("I have a dentist appointment tomorrow.",
     {"entities": [(0, 1, "CURRENT_USER")]}),
    ("Are those my keys?", {"entities": [(9, 11, "CURRENT_USER")]}),
    ("find all my issues", {"entities": [(9, 11, "CURRENT_USER")]}),
    ("get all project where I'm mentioned in any way", {"entities": [(21, 24, "CURRENT_USER")]}),
    ("search for all my merge requests", {"entities": [(9, 11, "CURRENT_USER")]}),
    ("search for comments mentioning me", {"entities": [(9, 11, "CURRENT_USER")]}),
    ("get me all comments that mention me", {"entities": [(9, 11, "CURRENT_USER")]}),
    ("show me all comments that mention me", {"entities": [(9, 11, "CURRENT_USER")]}),
    ("list comments that mention me", {"entities": [(9, 11, "CURRENT_USER")]}),

]

# find code with this `code`

# To generate more examples, consider variations in sentence structures,
# verb tenses, negations, questions, and complex sentences.
