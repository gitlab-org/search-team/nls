import spacy
import random

# Load an existing model
nlp = spacy.load("en_core_web_sm")

# Get the NER component from the pipeline
ner = nlp.get_pipe("ner")

# Add the custom label to the NER component
ner.add_label("ANIMAL")

# Disable the other pipeline components during training
other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "ner"]
with nlp.disable_pipes(*other_pipes):
    # Train the model with the custom label
    for i in range(10):
        random.shuffle(TRAIN_DATA)
        for text, annotations in TRAIN_DATA:
            doc = nlp.make_doc(text)
            example = spacy.training.Example.from_dict(doc, annotations)
            nlp.update([example])

nlp.to_disk("custom_ner_model")


