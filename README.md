# NLS - Natural Language Search

This project is seeking to search with natural lenguage. Using named entity recognition (NER).
Idea is to recognize a few basic key words in the natural language prompt input and use those keywords to construct a API call to elastic search or URL opening the search page directly.


## Getting started

init venv:
```bash 
  python3 -m venv NLS
```

```bash
  source NLS/bin/activate
```

install NLP lib and language model:
```bash 
  pip install requests spacy  
  python -m spacy download en_core_web_sm
```

run the code:
```bash
  python NLS_project.py
```

## Code

SpaCy with the `en_core_web_sm` language model currently only recognizes following labels: 

- `PERSON`: people, including fictional characters
- `NORP`: nationalities or religious or political groups
- `FAC`: buildings, airports, highways, bridges, etc.
- `ORG`: companies, agencies, institutions, etc.
- `GPE`: countries, cities, states
- `LOC`: non-GPE locations, mountain ranges, bodies of water
- `PRODUCT`: objects, vehicles, foods, etc. (not services)
- `EVENT`: named hurricanes, battles, wars, sports events, etc.
- `DATE`: absolute or relative dates or periods
- `TIME`: times smaller than a day

My idea is to add a few custom labels:

- `CURRENT_USER`: I, me, my, mine, when ever we talk about our selfs
- `SCOPE`: Code, Issues, Merge requests, Wiki, Commits, Comments, Milestones, Users,
this should be the easiest as we only have about 8 or 10 scopes
- `FILTERS`: I think this shoul start with just 'all' or default (first 10)
- `SORT`: some basic sorting (e.g. oldest first)

- `PROJECT`: I'm not sure how to recognize this without explicitly saying: in the project ...
- `GROUP`: Same as the project.

## Training

### Resources

- <https://spacy.io/usage/training#ner>
- <https://huggingface.co/datasets>
- <https://labelstud.io/>

### Current state

I've been exploring some avenues here. I tried some labeling tools. Hugging face has a free autotrain. I think that could be one how to do it. 
I tried to label some data using label-studio. But I'm not sure if the format of data provided is compatible with spaCy.
