import spacy

# Load the spaCy model
nlp = spacy.load("en_core_web_sm")


def extract_location(user_question):
    doc = nlp(user_question)
    for ent in doc.ents:
        if ent.label_ == "GPE":
            return ent.text
        if ent.label_ == "PERSON":
            return ent.text
    return None


# Get user's question from the command line prompt
user_question = input("Please enter your question: ")

# Extract location from the user's question
location = extract_location(user_question)

if location:
    print(f"Extracted location: {location}")
else:
    print("Could not extract location from the user's question.")
